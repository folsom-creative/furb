<?php
/**
 * Custom Theme Functions
 *
 * @package Furbabies
 */

//Development Mode
$development_mode = true; // Change to false during production. True enables the ACF settings panel and WP debug. 
if($development_mode === false ) {
	define('WP_DEBUG', true);
	add_filter('acf/settings/show_admin', '__return_false');
}


// Setup ACF Options Page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

